package com.fatowlstudio.core.service.impl;


import com.fatowlstudio.core.ex.LoginNotAvailableException;
import com.fatowlstudio.core.model.UserDetails;
import com.fatowlstudio.core.model.dto.UserDetailsDto;
import com.fatowlstudio.core.repository.UserDetailsRepository;
import com.fatowlstudio.core.service.AccountService;
import com.fatowlstudio.core.service.UserDetailsService;
import com.fatowlstudio.mailing.model.dto.EmailParam;
import com.fatowlstudio.mailing.model.value.Email;
import com.fatowlstudio.mailing.service.MailingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation for {@link com.fatowlstudio.core.service.UserDetailsService} interface. See the interface for public
 * API documentation.
 */
@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    private final UserDetailsRepository userDetailsRepository;
    private final AccountService accountService;
    private final MailingService mailingService;

    public UserDetailsServiceImpl(UserDetailsRepository userDetailsRepository, AccountService accountService,
        MailingService mailingService) {
        this.userDetailsRepository = userDetailsRepository;
        this.accountService = accountService;
        this.mailingService = mailingService;
    }

    @Override
    public UserDetails createUser(final UserDetailsDto userDetailsForm) {
        // Check if user with provided email already exists. If not, just register.
        if (userDetailsRepository.findByEmail(userDetailsForm.getEmail()) != null)
            throw new LoginNotAvailableException();

        UserDetails newUser = new UserDetails();

        newUser.setEmail(userDetailsForm.getEmail());
        newUser.setAuthorities(accountService.retrieveBaseAuthority());

        String newPassword = accountService.generatePassword();
        LOGGER.trace(String.format("Generated new password for user %s: %s", userDetailsForm.getEmail(), newPassword));
        newUser.setPassword(accountService.hashPassword(newPassword));

        mailingService.sendEmail(
            Email.Type.NEW_ACCOUNT,
            newUser.getEmail(),
            new EmailParam(Email.ParamName.NEW_PASSWORD, newPassword)
        );

        return userDetailsRepository.save(newUser);
    }
}
