package com.fatowlstudio.core.service.impl;

import com.fatowlstudio.core.service.AccountService;
import com.fatowlstudio.core.utils.CodingUtils;
import com.fatowlstudio.security.config.WebSecurityConfig;
import com.fatowlstudio.security.model.Authority;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation for {@link AccountService} interface. See the interface for public
 * API documentation.
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    public AccountServiceImpl() {
    }

    @Override
    public String generatePassword() {
        return CodingUtils.generateShortUUID();
    }

    @Override
    public String hashPassword(final String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(WebSecurityConfig.BCRYPT_COST));
    }

    @Override
    public String retrieveBaseAuthority() {
        return Authority.USER;
    }
}
