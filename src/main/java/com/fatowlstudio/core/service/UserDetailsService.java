package com.fatowlstudio.core.service;

import com.fatowlstudio.core.model.UserDetails;
import com.fatowlstudio.core.model.dto.UserDetailsDto;

/**
 * Service responsible for operations on users.
 */
public interface UserDetailsService {

    /**
     * Creates in the database an user passed as a user parameter.
     *
     * @param userDetailsForm user to be persisted
     * @return representation of the persisted user
     */
    UserDetails createUser(UserDetailsDto userDetailsForm);
}