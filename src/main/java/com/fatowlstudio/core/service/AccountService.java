package com.fatowlstudio.core.service;

/**
 * Service responsible for more specific account related actions, as password managing etc.
 */
public interface AccountService {

    /**
     * Generates new random password for user.
     *
     * @return random password
     */
    String generatePassword();

    /**
     * Generates BCrypted hash of password passed as a parameter and returns that hash.
     * @return BCrypt hash of password
     */
    String hashPassword(final String password);

    /**
     * Returns role name considered as the most basic for any new user.
     *
     * @return new user base authority
     */
    String retrieveBaseAuthority();
}
