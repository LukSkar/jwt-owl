package com.fatowlstudio.core.utils;

import org.springframework.data.domain.PageRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Builder for {@link org.springframework.data.domain.PageRequest} object used in Spring Data pagination.
 */
public abstract class PageRequestBuilder {

    public static final String PAGE_NUM_KEY = "pageNum";
    public static final String PER_PAGE_ELEMENTS_KEY = "perPageElements";

    /**
     * Builds {@link PageRequest} from passed page string and sort string. Page string should be in form of x,y where x is
     * number of requested page and y is elements per page, for example to get 3rd page with 10 elements pageString should
     * be 2,10 (pages are counted from 0). See {@link SortRequestBuilder#build(String)} for more information about sort
     * string. Sort string may be null, in that case no sorting will be built.
     *
     * @param pageString page string from which page request will be build
     * @param sortString sort string from which sorting options will be build
     * @return PageRequest object
     */
    public static PageRequest build(String pageString, String sortString) {
        Map<String, Integer> pageParams = getPageParams(pageString);
        int pageNum = pageParams.get(PAGE_NUM_KEY);
        int perPageElements = pageParams.get(PER_PAGE_ELEMENTS_KEY);
        if (sortString == null)
            return PageRequest.of(pageNum, perPageElements);
        else
            return PageRequest.of(pageNum, perPageElements, SortRequestBuilder.build(sortString));
    }

    /**
     * Creates a map with parameters for paging. Number of requested page will be stored at the {@link PageRequestBuilder#PAGE_NUM_KEY}
     * and requested elements per page will be stored at the {@link PageRequestBuilder#PER_PAGE_ELEMENTS_KEY}.
     *
     * @param pageString page string from which map parameters will be build
     * @return map with parameters for paging
     */
    private static Map<String, Integer> getPageParams(String pageString) {
        String[] pageStringParts = pageString.split(",");
        Map<String, Integer> pageParams = new HashMap<>();
        pageParams.put(PAGE_NUM_KEY, Integer.parseInt(pageStringParts[0]));
        pageParams.put(PER_PAGE_ELEMENTS_KEY, Integer.parseInt(pageStringParts[1]));
        return pageParams;
    }
}