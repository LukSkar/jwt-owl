package com.fatowlstudio.core.utils;

import org.springframework.data.domain.Sort;

/**
 * Builder for {@link org.springframework.data.domain.Sort} object used in Spring Data pagination.
 */
public abstract class SortRequestBuilder {

    /**
     * Builds {@link Sort} from passed sort string. Sort string should be in form of x:y where x is the field name and y
     * is the sorting order, eg. to sort descending by id sort string should be id:DESC. More than one sorting can be
     * specified, each sorting option should be separated by a comma, eg. id:DESC,author:ASC,rating:ASC.
     *
     * @param sortString sort string from which sorting options will be build
     * @return Sort object
     */
    public static Sort build(String sortString) {
        if (sortString == null || sortString.isEmpty())
            return new Sort(Sort.Direction.ASC, "id");

        // Get first sorting before loop as we can't create empty Sort to do it later with loop/stream
        String[] sorts = sortString.split(",");
        String[] firstSortParts = sorts[0].split(":");
        Sort sortObject = new Sort(directionFrom(firstSortParts[1]), firstSortParts[0]);

        for (int i = 1; i < sorts.length; i++) {
            String[] sortParts = sorts[i].split(":");
            sortObject = sortObject.and(new Sort(directionFrom(sortParts[1]), sortParts[0]));
        }

        return sortObject;
    }

    /**
     * Transforms string literals asc or desc into appropriate equivalent of the {@link org.springframework.data.domain.Sort.Direction}
     * type.
     *
     * @param directionString literal asc or desc
     * @return Springs Direction object for provided directionString
     */
    private static Sort.Direction directionFrom(String directionString) {
        if (directionString.toLowerCase().equals("asc"))
            return Sort.Direction.ASC;
        if (directionString.toLowerCase().equals("desc"))
            return Sort.Direction.DESC;

        return null;
    }
}