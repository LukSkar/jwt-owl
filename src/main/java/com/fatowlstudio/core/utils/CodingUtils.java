package com.fatowlstudio.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

/**
 * Abstract helper class providing some commonly used logic for encoding and decoding Strings.
 */
public abstract class CodingUtils {

    /**
     * Decodes provided base64 string to human readable utf8 form.
     *
     * @param text base64 string to be decoded
     * @return decoded string in utf8
     */
    public static String decodeFromBase64(String text) {
        byte[] decodedText = Base64.getDecoder().decode(text);
        return new String(decodedText, StandardCharsets.UTF_8);
    }

    /**
     * Encodes provided utf8 string to base64 form of it.
     *
     * @param text utf8 string to be encoded
     * @return base64 encoded string
     */
    public static String encodeToBase64(String text) {
        return Base64.getEncoder().encodeToString(text.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Generates short UUID, which is first 8 letters of regular UUID.
     *
     * @return first 8 letters of generated UUID
     */
    public static String generateShortUUID() {
        return UUID.randomUUID().toString().split("-")[0];
    }

    /**
     * Generates long UUID, which is just regular java randomUUID. Method created for consistency sake.
     *
     * @return UUID
     */
    public static String generateLongUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * Removes diacritics (accents) from a string. The case will not be altered. It calls standard apache
     * {@link StringUtils#stripAccents(String)} method and then also replaces some characters that are not
     * properly handled by it.
     *
     * @param text text to be stripped
     * @return input text with diacritics removed
     */
    public static String stripAccents(String text) {
        return StringUtils.stripAccents(text)
                .replaceAll("Ł", "L")
                .replaceAll("ł", "l")
                .replaceAll("Ø", "O");
    }
}
