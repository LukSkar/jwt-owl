package com.fatowlstudio.core.ex;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception thrown when user tries to register with use of non-available login.
 */
@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Login not available")
public class LoginNotAvailableException extends RuntimeException {
}
