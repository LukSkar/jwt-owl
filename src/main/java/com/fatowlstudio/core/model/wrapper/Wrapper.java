package com.fatowlstudio.core.model.wrapper;

/**
 * Simple primitive type wrapper, used to wrap primitive types when they are only one thing to return from controller,
 * so they can be used as a regular JSON object.
 */
public class Wrapper<T> {

    private T value;

    public Wrapper(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
