package com.fatowlstudio.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Model for storing users details in the database.
 */
@Entity
public class UserDetails {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(unique = true, nullable = false)
    private String email;

    @JsonIgnore
    @Column(columnDefinition = "text")
    private String password;

    @JsonIgnore
    @Column(nullable = false)
    private String authorities;

    /**
     * Standard constructor.
     */
    public UserDetails() {
    }

    /*
     * GETTERS AND SETTERS
     */
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthorities() {
        return authorities;
    }

    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }
}
