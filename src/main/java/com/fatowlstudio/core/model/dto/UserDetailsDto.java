package com.fatowlstudio.core.model.dto;

import com.fatowlstudio.core.model.UserDetails;

/**
 * General purpose dto for {@link UserDetails} entity class.
 */
public class UserDetailsDto {

    private String email;

    public UserDetailsDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
