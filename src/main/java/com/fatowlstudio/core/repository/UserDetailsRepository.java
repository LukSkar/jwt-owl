package com.fatowlstudio.core.repository;

import com.fatowlstudio.core.model.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository responsible for operations on {@link com.fatowlstudio.core.model.UserDetails}.
 */
public interface UserDetailsRepository extends JpaRepository<UserDetails, Long> {

    /**
     * Finds user by email.
     *
     * @param email email of user we search for
     * @return user with provided email
     */
    @Query("SELECT ud FROM UserDetails ud WHERE ud.email = :email")
    UserDetails findByEmail(@Param("email") String email);
}
