package com.fatowlstudio.core.config;

import com.fatowlstudio.security.config.WebSecurityConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.ZonedDateTime;
import java.util.Collections;

/**
 * Swagger configuration. It will pick up any Spring @RestController and all of its actions
 * and automatically set it up to use for use by swagger ui. Springfox implementation is used.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Creates standard Springfox bean for swagger.
     *
     * @return Springfox Docket
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .directModelSubstitute(ZonedDateTime.class, String.class)
                .select()
                    .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                    .paths(PathSelectors.any())
                .build()
                    .globalOperationParameters(
                        Collections.singletonList(
                            new ParameterBuilder()
                                .name(WebSecurityConfig.JWT_TOKEN_HEADER)
                                .description("Authorization header")
                                .modelRef(new ModelRef("string"))
                                .parameterType("header")
                                .required(false)
                                .build()
                        )
                    );
    }
}
