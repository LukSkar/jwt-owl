package com.fatowlstudio.core.api;

/**
 * Abstract controller class from which other controllers should inherit. This class contains constants for REST paths.
 */
public abstract class BaseController {

    // General api prefix
    private static final String     API_PREFIX =            "/api";

    // Authentication controller
    private static final String     AUTH_PREFIX =           API_PREFIX + "/auth";
    public static final String      LOGIN =                 AUTH_PREFIX + "/login";
    public static final String      LOGOUT =                AUTH_PREFIX + "/logout";

    // Users controller
    public static final String      USERS =                 API_PREFIX + "/users";

    // Uploads controller
    public static final String      UPLOADS =               API_PREFIX + "/uploads";
}
