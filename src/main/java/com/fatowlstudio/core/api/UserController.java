package com.fatowlstudio.core.api;

import com.fatowlstudio.core.model.UserDetails;
import com.fatowlstudio.core.model.dto.UserDetailsDto;
import com.fatowlstudio.core.service.UserDetailsService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Controller responsible for operations on users.
 */
@RestController
public class UserController extends BaseController {

    private final UserDetailsService userDetailsService;

    /**
     * Standard constructor.
     *
     * @param userDetailsService autowired userDetailsService
     */
    public UserController(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    /**
     * API bridge for {@link UserDetailsService#createUser(UserDetailsDto)}. See it for more info.
     */
    @ApiOperation(
        value = "createUser",
        notes = "Registers user based on provided request body. Password will be generated automatically. Created user " +
                "will be returned in response in case of successful processing."
    )
    @PostMapping(
        value = BaseController.USERS,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public UserDetails createUser(
        @Valid @RequestBody UserDetailsDto userDetailsForm
    ) {
        return userDetailsService.createUser(userDetailsForm);
    }
}
