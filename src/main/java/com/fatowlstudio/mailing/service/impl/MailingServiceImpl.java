package com.fatowlstudio.mailing.service.impl;

import com.fatowlstudio.mailing.model.EmailTemplate;
import com.fatowlstudio.mailing.model.dto.EmailParam;
import com.fatowlstudio.mailing.model.value.Email;
import com.fatowlstudio.mailing.repository.EmailTemplateRepository;
import com.fatowlstudio.mailing.service.MailingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation for {@link MailingService} interface. See it for public API documentation.
 */
@Service
@Transactional
public class MailingServiceImpl implements MailingService {

    // The email address that will be visible as a sender, taken from application.properties file. You need to have access
    // to that account - login, password, port and host are required, see configuration in the application.properties in mailing part.
    @Value("${spring.mail.username}") private String sender;

    private final JavaMailSender mailSender;
    private final EmailTemplateRepository templateRepository;

    /**
     * Standard constructor.
     *
     * @param mailSender autowired mail sender
     * @param templateRepository autowired template repository
     */
    public MailingServiceImpl(JavaMailSender mailSender, EmailTemplateRepository templateRepository) {
        this.mailSender = mailSender;
        this.templateRepository = templateRepository;
    }

    @Override
    @Async
    public void sendEmail(Email.Type emailType, String receiver, EmailParam... params) {
        final EmailTemplate emailTemplate = templateRepository.findByEmailType(emailType);
        mailSender.send(
            prepareEmail(
                receiver,
                emailTemplate.getTitle(),
                processContent(emailTemplate.getTemplate(), params))
        );
    }

    /**
     * Prepares {@link MimeMessagePreparator} for email with passed parameters to be used by java mailer client.
     *
     * @param receiver email receiver address
     * @param subject email subject
     * @param content email html content
     * @return ready to send mime email message
     */
    private MimeMessagePreparator prepareEmail(String receiver, String subject, String content) {
        return mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setFrom(sender);
            messageHelper.setTo(receiver);
            messageHelper.setSubject(subject);
            messageHelper.setText(content, true);
        };
    }

    /**
     * Processes email content replacing generic params with values.
     *
     * @param emailContent html content of generic email
     * @param params params to be injected into this email
     * @return processed html email content
     */
    private String processContent(String emailContent, EmailParam... params) {
        String processedContent = emailContent;
        for (EmailParam param : params) {
            processedContent = processedContent.replaceAll(param.getKey().name(), param.getValue());
        }
        return processedContent;
    }
}
