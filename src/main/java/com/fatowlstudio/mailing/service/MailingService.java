package com.fatowlstudio.mailing.service;

import com.fatowlstudio.mailing.model.dto.EmailParam;
import com.fatowlstudio.mailing.model.value.Email;

/**
 * Service responsible for sending emails.
 */
public interface MailingService {

    /**
     * Sends email.
     *
     * @param emailType type of email to be sent
     * @param receiver receiver of the email
     * @param params email params
     */
    void sendEmail(Email.Type emailType, String receiver, EmailParam... params);
}