package com.fatowlstudio.mailing.repository;

import com.fatowlstudio.mailing.model.EmailTemplate;
import com.fatowlstudio.mailing.model.value.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository responsible for operations on {@link EmailTemplate}.
 */
public interface EmailTemplateRepository extends JpaRepository<EmailTemplate, Long> {

    @Query("SELECT et FROM EmailTemplate et WHERE et.emailType = :emailType")
    EmailTemplate findByEmailType(@Param("emailType") Email.Type emailType);
}
