package com.fatowlstudio.mailing.model;

import com.fatowlstudio.mailing.model.value.Email;

import javax.persistence.*;

/**
 * Entity for storing email templates.
 */
@Entity
public class EmailTemplate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Email.Type emailType;

    @Column(columnDefinition = "text")
    private String title;

    @Column(columnDefinition = "text")
    private String template;

    public EmailTemplate() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Email.Type getEmailType() {
        return emailType;
    }

    public void setEmailType(Email.Type emailType) {
        this.emailType = emailType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
