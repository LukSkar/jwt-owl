package com.fatowlstudio.mailing.model.dto;

import com.fatowlstudio.mailing.model.value.Email;

/**
 * Class for transferring email parameters.
 */
public class EmailParam {

    private Email.ParamName key;
    private String value;

    public EmailParam(Email.ParamName key, String value) {
        this.key = key;
        this.value = value;
    }

    public Email.ParamName getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
