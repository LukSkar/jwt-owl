package com.fatowlstudio.mailing.model.value;

/**
 * Container class for storing possible email template types and email parameters
 */
public abstract class Email {

    public enum Type {
        NEW_ACCOUNT;
    }

    public enum ParamName {
        NEW_PASSWORD;
    }
}
