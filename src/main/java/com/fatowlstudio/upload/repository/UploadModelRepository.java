package com.fatowlstudio.upload.repository;

import com.fatowlstudio.upload.model.UploadModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Repository responsible for operations on {@link UploadModel}.
 */
public interface UploadModelRepository extends JpaRepository<UploadModel, Long> {

    /**
     * Retrieves list of unused upload models. Upload model is considered as unused if a file connected with this model
     * wasn't confirmed and it was uploaded before a date passed as a parameter.
     *
     * @return list of unused models
     */
    @Query("SELECT um FROM UploadModel um WHERE um.confirmed = false AND um.uploadDate < :limit")
    List<UploadModel> findUnusedModels(@Param("limit") ZonedDateTime limit);

    /**
     * Retrieves upload model with http resource link passed as a http link parameter.
     *
     * @param httpLink http resource link to search for
     * @return upload model with passed resource link
     */
    @Query("SELECT um FROM UploadModel um WHERE um.resourceHttpLink = :httpLink")
    UploadModel findByHttpResourceLink(@Param("httpLink") String httpLink);
}
