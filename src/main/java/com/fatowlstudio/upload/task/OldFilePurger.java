package com.fatowlstudio.upload.task;

import com.fatowlstudio.upload.service.UploadModelService;
import com.fatowlstudio.upload.service.UploadService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * CRON job responsible for removing uploaded but unused files. It will delete each non-confirmed file from filesystem
 * and the database if it it's older than two days.
 */
@Component
public class OldFilePurger {

    private final UploadModelService uploadModelService;
    private final UploadService uploadService;

    public OldFilePurger(UploadModelService uploadModelService, UploadService uploadService) {
        this.uploadModelService = uploadModelService;
        this.uploadService = uploadService;
    }

    /**
     * Purges old, unused files every day at 1:00 a.m.
     */
    @Scheduled(cron = "0 0 1 * * *")
    @Transactional
    public void purgeOldFiles() {
        uploadModelService.findUnusedUploadModels().forEach(uploadService::deleteFileWithModel);
    }
}
