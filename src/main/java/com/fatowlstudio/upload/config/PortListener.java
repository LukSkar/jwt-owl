package com.fatowlstudio.upload.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Listener class waiting for embedded servlet container to initialize so application port will be set. Port discovery is
 * done with help of this class as per time when @Value properties are injected port is not set and always equals 0. Set
 * port will be stored in the runningPort variable.
 */
@Component
public class PortListener implements ApplicationListener<ServletWebServerInitializedEvent> {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    public static int runningPort;

    @Override
    public void onApplicationEvent(ServletWebServerInitializedEvent event) {
        runningPort = event.getWebServer().getPort();
        LOGGER.info("Embedded servlet container initialized, port set to " + runningPort);
    }
}
