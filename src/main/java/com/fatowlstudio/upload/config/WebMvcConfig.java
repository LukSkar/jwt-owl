package com.fatowlstudio.upload.config;

import com.fatowlstudio.upload.model.value.Upload;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.PathResourceResolver;

/**
 * Web MVC configuration class. This class is used to configure resources paths and enable resources serving from Tomcat.
 */
//todo: This class should be moved to a security module, as an application without upload option still will require CORS config.
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(Upload.Dirs.RESOURCE_HTTP_PATH)
                .addResourceLocations("file:" + Upload.Dirs.RESOURCES_BASE + "/")
                .setCachePeriod(0)
                .resourceChain(false)
                .addResolver(new PathResourceResolver());
    }

    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowCredentials(false)
                .allowedHeaders("Content-Type", "Origin", "Accept", "X-Authorization")
                .allowedMethods("GET", "POST", "DELETE", "PUT", "OPTIONS")
                .allowedOrigins("*");
    }
}