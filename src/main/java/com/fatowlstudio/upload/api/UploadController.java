package com.fatowlstudio.upload.api;

import com.fatowlstudio.core.api.BaseController;
import com.fatowlstudio.upload.model.UploadModel;
import com.fatowlstudio.upload.service.UploadService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Controller responsible for uploading and managing files on the server.
 */
@RestController
public class UploadController extends BaseController {

    private final UploadService uploadService;

    /**
     * Standard constructor.
     *
     * @param uploadService autowired uploadService
     */
    public UploadController(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    /**
     * API bridge for {@link UploadService#uploadFile(MultipartFile)}. See it for more info.
     */
    @ApiOperation(
        value = "upload",
        notes = "Uploads multipart file to the server. Internal file location in the host file system will be decided " +
                "based on uploaded file type. Full model of uploaded file will be returned. Allowed extensions are: jpg, " +
                "png, pdf, mp4. File type is discovered automatically."
    )
    @PostMapping(
        value = BaseController.UPLOADS,
        consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public UploadModel upload(
        @RequestParam(value = "file", required = true) MultipartFile file
    ) throws IOException {
        return uploadService.uploadFile(file);
    }

}