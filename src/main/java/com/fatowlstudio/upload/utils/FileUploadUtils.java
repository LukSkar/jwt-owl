package com.fatowlstudio.upload.utils;

import com.fatowlstudio.upload.config.PortListener;
import com.fatowlstudio.upload.model.value.Upload;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Abstract helper class providing many operations for {@link com.fatowlstudio.upload.service.impl.UploadServiceImpl} file upload
 * service implementation and others.
 */
public abstract class FileUploadUtils {

    /**
     * Retrieves file extension from multipart file.
     *
     * @param file multipart file from which extension should be retrieved
     * @return extension of the provided file
     */
    public static String getFileExtension(MultipartFile file) {
        return FilenameUtils.getExtension(file.getOriginalFilename()).toLowerCase();
    }

    /**
     * Discovers file upload type based on uploaded multipart file.
     *
     * @param file file of which upload type should be discovered
     * @return upload type of provided file
     */
    public static Upload.Type determineFileUploadType(MultipartFile file) {
        switch (getFileExtension(file)) {
            case "jpg":
            case "png":
                return Upload.Type.IMAGE;
            case "pdf":
                return Upload.Type.PDF;
            case "mp4":
                return Upload.Type.VIDEO;
            default:
                throw new IllegalArgumentException("Unknown or not supported file type.");
        }
    }

    /**
     * Retrieves path to which file should be uploaded based on it's upload type.
     *
     * @param uploadType upload type of the file
     * @return path to which file should be uploaded
     */
    public static String determineFileTargetDirectory(Upload.Type uploadType) {
        switch (uploadType) {
            case IMAGE:
                return Upload.Dirs.IMAGES;
            case PDF:
                return Upload.Dirs.PDFS;
            case VIDEO:
                return Upload.Dirs.VIDEOS;
            default:
                throw new IllegalArgumentException("Unknown or not supported file type.");
        }
    }

    /**
     * Builds and returns http path to the uploaded file. It is the link at which file can be accessed by browsers.
     *
     * @param targetDirectory server directory at which file is stored
     * @param fileName filename
     * @return http link to the file
     * @throws UnknownHostException in case when application cannot specify host on which is running
     */
    public static String buildHttpPathToResource(File targetDirectory, String fileName, String httpHost, boolean isSSL) throws UnknownHostException {
        String protocol = isSSL ? "https://" : "http://";
        String httpHostProcessed = httpHost.equals("INET") ? InetAddress.getLocalHost().getHostAddress() : httpHost;
        String port = httpHost.equals("INET") ? ":".concat(Integer.toString(PortListener.runningPort)) : "";

        return protocol
                .concat(httpHostProcessed)
                .concat(port)
                .concat("/")
                .concat(targetDirectory.getPath().replace("\\", "/"))
                .concat("/")
                .concat(fileName);
    }
}
