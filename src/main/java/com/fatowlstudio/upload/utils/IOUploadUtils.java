package com.fatowlstudio.upload.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Helper class for performing IO operations while uploading files.
 */
public class IOUploadUtils {

    /**
     * Retrieves physical folder of target directory as a File object enabling all java.io file operations on it. If it
     * does not exist it will be created.
     *
     * @param targetDirectoryPath path of target directory
     * @return target directory as a File instance
     */
    public static File createDirectoryToWrite(String targetDirectoryPath) {
        File directory = new File(targetDirectoryPath);
        if (!directory.exists()) directory.mkdirs();
        return directory;
    }

    /**
     * Creates server file to which uploaded file can be written.
     *
     * @param targetDirectory directory in which file will be created
     * @param fileName name at which file will be created
     * @return empty server file ready to write
     */
    public static File createFileToWrite(File targetDirectory, String fileName) {
        File newServerFile = new File(
                targetDirectory.getAbsolutePath().concat(File.separator).concat(fileName)
        );

        if (newServerFile.exists())
            throw new IllegalStateException("Cannot upload file as it already exist on the server");

        return newServerFile;
    }

    /**
     * Physically writes uploaded file to the target file on server.
     *
     * @param targetFile file to which bytes should be written
     * @param uploadedFile bytes that should be written
     * @throws IOException in case of IO operations failure
     */
    public static void writeFile(File targetFile, MultipartFile uploadedFile) throws IOException {
        try (BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(targetFile))) {
            stream.write(uploadedFile.getBytes());
        }
    }
}
