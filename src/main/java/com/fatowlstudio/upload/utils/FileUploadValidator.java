package com.fatowlstudio.upload.utils;

import com.fatowlstudio.upload.ex.InvalidDataException;
import com.fatowlstudio.upload.model.value.Upload;
import org.springframework.web.multipart.MultipartFile;

/**
 * Validator for file uploads.
 */
public class FileUploadValidator {

    /**
     * Validates if file is properly prepared and valid. Exception will be thrown if not.
     *
     * @param file file to be validated
     */
    public static void validate(MultipartFile file) {
        if (!fileContainsData(file))
            throw new InvalidDataException("File cannot be empty.");
        if (!isFormatSupported(file))
            throw new InvalidDataException("Uploaded file type is not supported");
    }

    /**
     * Checks if file is not empty.
     *
     * @param file file to be checked
     * @return true if file is not empty, false otherwise
     */
    private static boolean fileContainsData(MultipartFile file) {
        return !file.isEmpty();
    }

    /**
     * Checks if uploaded file is one of supported formats.
     *
     * @param file file to be checked
     * @return true if file format is supported, false otherwise
     */
    private static boolean isFormatSupported(MultipartFile file) {
        return Upload.SupportedFormats.getSupportedFormats().contains(FileUploadUtils.getFileExtension(file));
    }
}
