package com.fatowlstudio.upload.ex;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception thrown when user provided invalid data (that can't be validated with JSR validation and is catched
 * with application logic, like change password fields don't matching).
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidDataException extends RuntimeException {

    public InvalidDataException(String message) {
        super(message);
    }
}
