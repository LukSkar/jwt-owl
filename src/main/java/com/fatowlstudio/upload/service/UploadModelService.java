package com.fatowlstudio.upload.service;

import com.fatowlstudio.upload.model.UploadModel;

import java.util.List;

/**
 * Service responsible for operations on {@link UploadModel} entities.
 */
public interface UploadModelService {

    /**
     * Creates in the database an upload model passed as an upload model data parameter.
     *
     * @param uploadModelData upload model to be persisted
     * @return representation of the persisted upload model
     */
    UploadModel createUploadModel(UploadModel uploadModelData);

    /**
     * Returns upload model corresponding to a http resource link passed as a parameter.
     *
     * @param httpLink http link to be searched for
     * @return upload model with requested http link
     */
    UploadModel findByHttpResourceLink(String httpLink);

    /**
     * Returns list of unused upload models. Upload model is considered to be unused if its older that two days and
     * still wasn't confirmed.
     *
     * @return list of unused uploads
     */
    List<UploadModel> findUnusedUploadModels();

    /**
     * Deletes upload model with passed id.
     *
     * @param modelId id of the model to delete
     */
    void remove(long modelId);

    /**
     * Sets upload model with passed id to confirmed state.
     *
     * @param modelId id of the model to be set to confirmed
     */
    void confirm(long modelId);
}
