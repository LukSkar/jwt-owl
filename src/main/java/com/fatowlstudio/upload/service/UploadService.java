package com.fatowlstudio.upload.service;

import com.fatowlstudio.upload.model.UploadModel;
import com.fatowlstudio.upload.model.value.Upload;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Service responsible for uploading and managing files on the server.
 */
public interface UploadService {

    /**
     * Uploads multipart file to the server. Internal file location in the host file system will be decided based on
     * uploaded file type - by default jpg's nad png's are uploaded to the uploads/images folder, mp4's are uploaded to
     * the uploads/videos folder and pdf's are uploaded to the uploads/pdfs folder. Other files are not supported by
     * default. Those default paths can be changed by modifying {@link Upload.Dirs} class.
     *
     * An entity of the uploaded file will be created in the {@link UploadModel} - it is used to purge unused files and
     * to store file locations and other file metadata.
     *
     * @param file file to be uploaded
     * @return uploaded file model
     * @throws IOException in case of IO processing error
     */
    UploadModel uploadFile(MultipartFile file) throws IOException;

    /**
     * Physically removes file with provided file path from filesystem.
     *
     * @param path path of file to be removed
     */
    void deleteFile(String path);

    /**
     * Physically removes file bind with provided upload model from filesystem.
     *
     * @param uploadModel upload model of file to be removed
     */
    void deleteFile(UploadModel uploadModel);

    /**
     * Physically removes file bind with provided upload model and removes that upload model.
     *
     * @param uploadModel upload model to be removed with it's file
     */
    void deleteFileWithModel(UploadModel uploadModel);
}
