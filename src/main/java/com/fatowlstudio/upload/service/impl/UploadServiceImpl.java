package com.fatowlstudio.upload.service.impl;

import com.fatowlstudio.core.utils.CodingUtils;
import com.fatowlstudio.upload.model.UploadModel;
import com.fatowlstudio.upload.model.value.Upload;
import com.fatowlstudio.upload.service.UploadModelService;
import com.fatowlstudio.upload.service.UploadService;
import com.fatowlstudio.upload.utils.FileUploadUtils;
import com.fatowlstudio.upload.utils.FileUploadValidator;
import com.fatowlstudio.upload.utils.IOUploadUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Default implementation for {@link UploadService} interface. See the interface for public API documentation.
 */
@Service
@Transactional
public class UploadServiceImpl implements UploadService {

    @Value("${app.host}")
    private String appHost;

    @Value("${app.ssl}")
    private boolean appSSL;

    private final UploadModelService uploadModelService;

    /**
     * Standard constructor.
     *
     * @param uploadModelService autowired UploadModelService
     */
    public UploadServiceImpl(UploadModelService uploadModelService) {
        this.uploadModelService = uploadModelService;
    }

    @Override
    public UploadModel uploadFile(MultipartFile file) throws IOException {

        FileUploadValidator.validate(file);

        Upload.Type uploadType =        FileUploadUtils.determineFileUploadType(file);
        String fileFormat =             FileUploadUtils.getFileExtension(file);
        String fileName =               CodingUtils.generateShortUUID().concat(".").concat(fileFormat);

        String targetDirectoryPath =    FileUploadUtils.determineFileTargetDirectory(uploadType);
        File targetDirectory =          IOUploadUtils.createDirectoryToWrite(targetDirectoryPath);
        File targetFile =               IOUploadUtils.createFileToWrite(targetDirectory, fileName);

        String httpPath =               FileUploadUtils.buildHttpPathToResource(targetDirectory, fileName, appHost, appSSL);

        IOUploadUtils.writeFile(targetFile, file);
        return uploadModelService.createUploadModel(
            new UploadModel(
                httpPath, targetFile.getAbsolutePath(), uploadType, fileFormat, targetFile.length() / 1024L, fileName
            )
        );
    }

    @Override
    public void deleteFile(String path) {
        try {
            Files.deleteIfExists(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteFile(UploadModel uploadModel) {
        deleteFile(uploadModel.getResourcePhysicalLocation());
    }

    @Override
    public void deleteFileWithModel(UploadModel uploadModel) {
        uploadModelService.remove(uploadModel.getId());
        deleteFile(uploadModel.getResourcePhysicalLocation());
    }
}
