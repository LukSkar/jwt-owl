package com.fatowlstudio.upload.service.impl;

import com.fatowlstudio.upload.model.UploadModel;
import com.fatowlstudio.upload.repository.UploadModelRepository;
import com.fatowlstudio.upload.service.UploadModelService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * Default implementation of the {@link UploadModelService}. See it for public API documentation.
 */
@Service
@Transactional
public class UploadModelServiceImpl implements UploadModelService {

    private final UploadModelRepository uploadModelRepository;

    /**
     * Standard constructor.
     *
     * @param uploadModelRepository autowired UploadModelRepository
     */
    public UploadModelServiceImpl(UploadModelRepository uploadModelRepository) {
        this.uploadModelRepository = uploadModelRepository;
    }

    @Override
    public UploadModel createUploadModel(UploadModel uploadModelData) {
        return uploadModelRepository.save(uploadModelData);
    }

    @Override
    public UploadModel findByHttpResourceLink(String httpLink) {
        return uploadModelRepository.findByHttpResourceLink(httpLink);
    }

    @Override
    public List<UploadModel> findUnusedUploadModels() {
        return uploadModelRepository.findUnusedModels(ZonedDateTime.now(ZoneOffset.UTC).minus(2L, ChronoUnit.DAYS));
    }

    @Override
    public void remove(long modelId) {
        uploadModelRepository.deleteById(modelId);
    }

    @Override
    public void confirm(long modelId) {
        //todo: get without present check
        UploadModel toConfirm = uploadModelRepository.findById(modelId).get();
        toConfirm.setConfirmed(true);
        uploadModelRepository.save(toConfirm);
    }
}
