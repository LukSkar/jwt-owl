package com.fatowlstudio.upload.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fatowlstudio.upload.model.value.Upload;

import javax.persistence.*;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * Model for storing nad transferring data produced during file upload.
 */
@Entity
public class UploadModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(columnDefinition = "text", nullable = false, unique = true)
    private String resourceHttpLink;

    @Column(columnDefinition = "text", nullable = false, unique = true)
    @JsonIgnore
    private String resourcePhysicalLocation;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Upload.Type uploadType;

    @Column(nullable = false)
    private String format;

    @Column(nullable = false)
    @JsonIgnore
    private boolean confirmed;

    @Column(nullable = false)
    @JsonIgnore
    private ZonedDateTime uploadDate;

    @Column(name = "kb_file_size")
    private long kBfileSize;

    private String fileName;

    public UploadModel() {
    }

    public UploadModel(String resourceHttpLink, String resourcePhysicalLocation, Upload.Type uploadType, String format,
                       boolean confirmed, ZonedDateTime uploadDate, long kBfileSize, String fileName) {
        this.resourceHttpLink = resourceHttpLink;
        this.resourcePhysicalLocation = resourcePhysicalLocation;
        this.uploadType = uploadType;
        this.format = format;
        this.confirmed = confirmed;
        this.uploadDate = uploadDate;
        this.kBfileSize = kBfileSize;
        this.fileName = fileName;
    }

    public UploadModel(String resourceHttpLink, String resourcePhysicalLocation, Upload.Type uploadType, String format,
                       long kBfileSize, String fileName) {
        this(resourceHttpLink, resourcePhysicalLocation, uploadType, format, false, ZonedDateTime.now(ZoneOffset.UTC),
                kBfileSize, fileName);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getResourceHttpLink() {
        return resourceHttpLink;
    }

    public void setResourceHttpLink(String resourceHttpLink) {
        this.resourceHttpLink = resourceHttpLink;
    }

    public String getResourcePhysicalLocation() {
        return resourcePhysicalLocation;
    }

    public void setResourcePhysicalLocation(String resourcePhysicalLocation) {
        this.resourcePhysicalLocation = resourcePhysicalLocation;
    }

    public Upload.Type getUploadType() {
        return uploadType;
    }

    public void setUploadType(Upload.Type uploadType) {
        this.uploadType = uploadType;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public ZonedDateTime getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(ZonedDateTime uploadDate) {
        this.uploadDate = uploadDate;
    }

    public long getkBfileSize() {
        return kBfileSize;
    }

    public void setkBfileSize(long kBfileSize) {
        this.kBfileSize = kBfileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
