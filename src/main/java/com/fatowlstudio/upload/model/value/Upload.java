package com.fatowlstudio.upload.model.value;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Abstract container for enums and constants associated with uploads.
 */
public abstract class Upload {

    public static abstract class Dirs {
        public static String RESOURCE_HTTP_PATH = "/uploads/**";
        public static String RESOURCES_BASE = "uploads";

        public static String IMAGES = RESOURCES_BASE + File.separator.concat("images").concat(File.separator);
        public static String VIDEOS = RESOURCES_BASE + File.separator.concat("videos").concat(File.separator);
        public static String PDFS =   RESOURCES_BASE + File.separator.concat("pdfs").concat(File.separator);
    }

    public static abstract class SupportedFormats {
        private static String JPG = "jpg";
        private static String PNG = "png";
        private static String MP4 = "mp4";
        private static String PDF = "pdf";

        public static List<String> getSupportedFormats() {
            return Arrays.asList(JPG, PNG, MP4, PDF);
        }
    }

    public enum Type {
        IMAGE, VIDEO, PDF
    }
}