package com.fatowlstudio.security.ex;

import org.springframework.security.core.AuthenticationException;

/**
 * Exception signalising that signature of parsed JWT token is not valid. To be thrown in Spring Security
 * authentication providers.
 */
public class InvalidJwtSignatureException extends AuthenticationException {

    public InvalidJwtSignatureException(String msg, Throwable t) {
        super(msg, t);
    }
}
