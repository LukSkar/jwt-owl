package com.fatowlstudio.security.ex;

import org.springframework.security.core.AuthenticationException;

/**
 * Exception signalising general issue with JWT token processing. To be thrown in Spring Security
 * authentication providers.
 */
public class JwtProcessingException extends AuthenticationException {

    public JwtProcessingException(String msg, Throwable t) {
        super(msg, t);
    }
}
