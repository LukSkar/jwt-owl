package com.fatowlstudio.security.api;

import com.fatowlstudio.core.api.BaseController;
import com.fatowlstudio.core.model.wrapper.Wrapper;
import com.fatowlstudio.security.model.LoginRequest;
import com.fatowlstudio.security.service.AuthService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Controller responsible for logging users in and out.
 */
@RestController
public class AuthController extends BaseController {

    private final AuthService authService;

    /**
     * Standard constructor.
     *
     * @param authService autowired authService
     */
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    /**
     * API bridge for {@link AuthService#authorize(LoginRequest)}. See it for more details.
     */
    @ApiOperation(
        value = "login",
        notes = "Generates JWT token for user with <b>login</b> and <b>password</b> provided as a LoginRequest and logs " +
                "that user in. Will return 401 in case of login failure."
    )
    @PostMapping(
        value = BaseController.LOGIN,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Wrapper<String>> login(
        @Valid @RequestBody LoginRequest loginRequest
    ) {
        String token = authService.authorize(loginRequest);
        if (token != null)
            return new ResponseEntity<>(new Wrapper<>(token), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * API bridge for {@link AuthService#logout()}. See it for more details.
     */
    @ApiOperation(
        value = "logout",
        notes = "Devalidates provided token, effectively logging owning it user off."
    )
    @PostMapping(
        value = BaseController.LOGOUT,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("@permissionEvaluator.anyAuthenticated()")
    public void logout() {
        authService.logout();
    }
}
