package com.fatowlstudio.security.model;

/**
 * Model representing login request. Its immutable. One of the two: password or facebookAccessToken is mandatory. If
 * both provided the facebookAccessToken will take precedence and login request will be treated as social login request.
 */
public class LoginRequest {

    private String login;

    private String password;

    public LoginRequest() {

    }

    public LoginRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    /*
     * GETTERS AND SETTERS
     */

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
