package com.fatowlstudio.security.model;

import java.util.Date;

/**
 * Model for storing object representation of the JWT token. Its immutable.
 */
public class JwtToken {

    private String token;

    private String subject;

    private String authorities;

    private String tokenId;

    private Date issuedAt;

    private long userId;

    /**
     * Standard constructor
     *
     * @param token raw JWT token
     * @param subject sub claim
     * @param authorities rol claim
     * @param tokenId jti claim
     * @param issuedAt iat claim
     * @param userId uid claim
     */
    public JwtToken(String token, String subject, String authorities, String tokenId, Date issuedAt, long userId) {
        this.token = token;
        this.subject = subject;
        this.authorities = authorities;
        this.tokenId = tokenId;
        this.issuedAt = issuedAt;
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public String getSubject() {
        return subject;
    }

    public String getAuthorities() {
        return authorities;
    }

    public String getTokenId() {
        return tokenId;
    }

    public Date getIssuedAt() {
        return issuedAt;
    }

    public long getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return subject;
    }
}
