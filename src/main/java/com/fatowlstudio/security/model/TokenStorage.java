package com.fatowlstudio.security.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract class being in-memory container for tokens signed by the server. Every created token has its id, which is
 * stored here. When request arrives JWT provided with it is checked against base of signed tokens - if its not found it
 * will be treated as invalid. Logging out also removes token of logged out user from this storage disabling further use
 * of that token. This class contains some static helper methods to operate on storage.
 */
public abstract class TokenStorage {

    /**
     * Token storage. Its a map of tokens, where the key is a token (tokens are unique) and value is an id of the user
     * to which this token belongs.
     */
    private static Map<String, Long> tokenStorage = new HashMap<>();

    /**
     * Saves token in the token store.
     *
     * @param token token to be saved
     * @param userId id of the user to which this token belongs
     */
    public static void putTokenInStore(String token, Long userId) {
        tokenStorage.put(token, userId);
    }

    /**
     * Removes token from the token store. If there is no such token nothing will happen.
     *
     * @param token token id to be removed
     */
    public static void removeTokenFromStore(String token) {
        if (contains(token))
            tokenStorage.remove(token);
    }

    /**
     * Removes from the token store all tokens belonging to the user with passed user id.
     *
     * @param userId id of the user whose tokens should be removed
     */
    public static void removeAllUserTokensFromStore(Long userId) {
        tokenStorage.values().removeAll(Collections.singleton(userId));
    }

    /**
     * Checks if token storage contains provided token.
     *
     * @param token token id to be checked
     * @return true if storage contains that id, else otherwise
     */
    public static boolean contains(String token) {
        return tokenStorage.containsKey(token);
    }
}
