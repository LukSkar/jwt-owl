package com.fatowlstudio.security.model;

import com.fatowlstudio.security.service.PermissionEvaluator;
import org.springframework.stereotype.Component;

/**
 * Container for storing default authorities values. This class has to be non-abstract so Spring can put it into container
 * for later use by {@link PermissionEvaluator} but it's constructor is private so it can't be declared explicitly in
 * code by parties other than Spring.
 */
@Component("authority")
public class Authority {

    private Authority() {

    }

    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
}
