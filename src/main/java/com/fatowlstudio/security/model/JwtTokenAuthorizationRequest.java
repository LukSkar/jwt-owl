package com.fatowlstudio.security.model;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Spring Security authentication class. It holds authentication data used when authenticating by
 * {@link com.fatowlstudio.security.provider.JwtAuthenticationProvider} and by Spring Security chain after
 * successful authorization.
 */
public class JwtTokenAuthorizationRequest extends AbstractAuthenticationToken {

    private String rawJwtToken;
    private JwtToken objectJwtToken;

    /**
     * Constructor used when passing object to be authenticated. Its called in
     * {@link com.fatowlstudio.security.filter.JwtAuthenticationFilter#attemptAuthentication} and works with
     * {@link com.fatowlstudio.security.provider.JwtAuthenticationProvider} where token is parsed and validated.
     * JwtAuthenticationProvider will return new instance of it with authorities set by another constructor.
     * See appropriate authentication provider for more details.
     *
     * @param rawJwtToken string representing raw jwt token retrieved form request
     */
    public JwtTokenAuthorizationRequest(String rawJwtToken) {
        super(null);
        this.rawJwtToken = rawJwtToken;
        this.objectJwtToken = null;
        this.setAuthenticated(false);
    }

    /**
     * Constructor used by {@link com.fatowlstudio.security.provider.JwtAuthenticationProvider} to fill authorities
     * and mark this object as authenticated after successful token processing in the provider.
     *
     * @param jwtToken object representation of parsed raw jwt token
     * @param authorities authorities of the successfuly validated token
     */
    public JwtTokenAuthorizationRequest(JwtToken jwtToken, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.eraseCredentials();
        this.objectJwtToken = jwtToken;
        super.setAuthenticated(true);
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        if (authenticated)
            throw new IllegalArgumentException("Token cannot be set to trusted state outside of the appropriate authentication provider");
        super.setAuthenticated(false);
    }

    @Override
    public Object getCredentials() {
        return rawJwtToken;
    }

    @Override
    public Object getPrincipal() {
        return objectJwtToken;
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        rawJwtToken = null;
    }
}
