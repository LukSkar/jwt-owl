package com.fatowlstudio.security.config;

import com.fatowlstudio.core.api.BaseController;
import com.fatowlstudio.security.filter.JwtAuthenticationFilter;
import com.fatowlstudio.security.provider.JwtAuthenticationProvider;
import com.fatowlstudio.upload.model.value.Upload;
import com.fatowlstudio.websocket.config.WebSocketConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.http.HttpServletResponse;

/**
 * Web security configuration class. This class defines global security rules, configures user authentication options
 * and integrates everything with Spring Security. It also contains some bean definitions needed for JWT in order to
 * work with Spring Security.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private Logger LOGGER = LogManager.getLogger(this.getClass());

    // Name of the HTTP header containing JWT token
    public final static String JWT_TOKEN_HEADER = "X-Authorization";

    // Cost to determine number of BCrypt rounds for BCrypt password hashing
    public final static int BCRYPT_COST = 12;

    @Value("${app.swagger.enabled}")
    private boolean swaggerEnabled;

    /**
     * Security configuration method. It defines global rules for Spring Security. Secured endpoints are handled by
     * @PreAuthorize rule at controller method level so they should not be defined here. Endpoints with no @PreAuthorize
     * at method are validated against @PreAuthorize(isAuthenticated()) by default. If you want to make some method
     * accessible without authentication define it in the {@link WebSecurityConfig#configure(WebSecurity)} method as
     * ignored (see javadoc of it for more details).
     *
     * @param http the {@link HttpSecurity} to modify
     * @throws Exception if an error occurs
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .cors()
        .and()
            .csrf().disable()   // We don't need CSRF protection for JWT based authentication
            .exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint())
        .and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .authorizeRequests()
                .anyRequest().fullyAuthenticated()
        .and()
            .addFilterBefore(new JwtAuthenticationFilter(authenticationManager()), UsernamePasswordAuthenticationFilter.class);
    }

    /**
     * Web security configuration method. It is used to declare paths that should not be verified by Spring Security.
     * It may be useful for enabling login/register endpoints for everyone or allowing resources to load without
     * authentication (js, html, css, image files etc).
     *
     * @param webSecurity the {@link WebSecurity} to modify
     */
    @Override
    public void configure(WebSecurity webSecurity) {
        // Ignore publicly available endpoints
        webSecurity.ignoring()
                .antMatchers(HttpMethod.POST, BaseController.LOGIN)
                .antMatchers(HttpMethod.POST, BaseController.USERS)
                .antMatchers(HttpMethod.POST, BaseController.UPLOADS)
                .antMatchers(WebSocketConfig.WS_HANDSHAKE)
                .antMatchers(Upload.Dirs.RESOURCE_HTTP_PATH);


        // Ignore swagger endpoints and resources - for development purposes
        if (swaggerEnabled) {
            webSecurity.ignoring().antMatchers(
                    "/v2/api-docs",
                    "/configuration/ui",
                    "/swagger-resources/**",
                    "/configuration/security",
                    "/swagger-ui.html",
                    "/webjars/**"
            );
            LOGGER.trace("Swagger is enabled - added swagger endpoints to list of publicly available endpoints");
        }

        LOGGER.trace("Web security configuration: DONE");
    }

    /**
     * Configures authentication manager. All possible authentication providers should be registered here.
     *
     * @param authBuilder Spring Security AuthenticationManagerBuilder to configure
     */
    @Override
    protected void configure(AuthenticationManagerBuilder authBuilder) {
        authBuilder.authenticationProvider(defaultAuthenticationProvider());
    }

    /**
     * Entry point for users without token. We declare it to override default redirection to the login page as
     * for REST services default behavior doesn't make sense. Instead of redirect it just returns 401 code.
     *
     * @return 401 UNAUTHORIZED HTTP status
     */
    @Bean
    public AuthenticationEntryPoint restAuthenticationEntryPoint() {
        return (request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }

    /**
     * Returns default authentication provider. This method is static as default authentication provider may be needed
     * in other application modules that don't use default security chain but still should obtain this default provider
     * as it is only secure way of authenticating users.
     *
     * @return default authentication provider
     */
    @Bean
    public static AuthenticationProvider defaultAuthenticationProvider() {
        return new JwtAuthenticationProvider();
    }
}
