package com.fatowlstudio.security.provider;

import com.fatowlstudio.security.ex.InvalidJwtSignatureException;
import com.fatowlstudio.security.ex.JwtProcessingException;
import com.fatowlstudio.security.model.JwtToken;
import com.fatowlstudio.security.model.JwtTokenAuthorizationRequest;
import com.fatowlstudio.security.model.TokenStorage;
import com.fatowlstudio.security.service.TokenService;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;

/**
 * Spring Security authentication provider for {@link com.fatowlstudio.security.model.JwtTokenAuthorizationRequest} class.
 * Its responsible for creating authenticated Authentication object from provided JwtTokenAuthorizationRequest and pass it
 * for further processing in Spring Security chain. It have to be registered and called from {@link com.fatowlstudio.security.filter.JwtAuthenticationFilter}
 *
 * Fields of this class are autowired at field level because Spring Security filters are instantiated before regular beans,
 * so they wont be available at the time of this filter constructor call for constructor injection. They will be injected
 * in a later lifecycle phase when they are ready.
 */
@SuppressWarnings({"SpringAutowiredFieldsWarningInspection", "SpringJavaAutowiredMembersInspection"})
public class JwtAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private TokenService tokenService;

    /**
     * Validates provided authentication object.
     *
     * @param authentication authentication object to be validated
     * @return new authentication object filled with authorities in case of successful validation
     * @throws AuthenticationException in case of validation error
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String rawToken = (String) authentication.getCredentials();
        if (rawToken == null || rawToken.equals("") || !rawToken.startsWith(tokenService.getTokenPrefix()))
           throw new InsufficientAuthenticationException("No authentication data were found");

        JwtToken jwtToken;
        try {
            jwtToken = tokenService.parseToken(rawToken);
            if (!TokenStorage.contains(jwtToken.getToken()))
                throw new AuthenticationCredentialsNotFoundException("The signature of provided token is valid, but it is " +
                    "not stored in signed tokens storage - probably user logged off or token expired. Try logging in again.");
        } catch (SignatureException e) {
            throw new InvalidJwtSignatureException("JWT signature verification failed.", e);
        } catch (JwtException e) {
            throw new JwtProcessingException("Authentication ex while parsing token - make sure provided token is " +
                    "properly constructed and filled with all required data.", e);
        }

        return new JwtTokenAuthorizationRequest(jwtToken,
                AuthorityUtils.commaSeparatedStringToAuthorityList(jwtToken.getAuthorities()));
    }

    /**
     * Checks if this authentication provider is a valid provider for an authentication object passed to it by Spring Security.
     *
     * @param authenticationClass class of authentication object
     * @return true if this provider can authenticate object of provided class
     */
    @Override
    public boolean supports(Class<?> authenticationClass) {
        return authenticationClass.equals(JwtTokenAuthorizationRequest.class);
    }
}
