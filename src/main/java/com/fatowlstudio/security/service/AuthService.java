package com.fatowlstudio.security.service;

import com.fatowlstudio.security.model.JwtToken;
import com.fatowlstudio.security.model.LoginRequest;

/**
 * Service responsible for authorization, logging users in nad out.
 */
public interface AuthService {

    /**
     * Generates JWT token for user with username and password provided as a {@link LoginRequest} and logs that user in.
     *
     * @param loginRequest object representing user that want to log in
     * @return JWT token if user successfully logged in, null otherwise
     */
    String authorize(LoginRequest loginRequest);

    /**
     * Invalidates token of user making this request, effectively logging owning it user off.
     */
    void logout();

    /**
     * Retrieves principal of user making request. Principal is {@link JwtToken} class. May return null if requesting user
     * is anonymous.
     *
     * @return requesting user security principal or null if requesting user is anonymous
     */
    JwtToken getLoggedUserPrincipal();
}
