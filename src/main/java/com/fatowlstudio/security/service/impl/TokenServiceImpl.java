package com.fatowlstudio.security.service.impl;

import com.fatowlstudio.core.model.UserDetails;
import com.fatowlstudio.security.model.JwtToken;
import com.fatowlstudio.security.service.TokenService;
import com.fatowlstudio.core.utils.CodingUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

/**
 * Default implementation for {@link com.fatowlstudio.security.service.TokenService} interface. See the interface for
 * public API documentation.
 */
@Service
public class TokenServiceImpl implements TokenService {

    /**
     * Base64 encoded secret key used for JWT tokens signing and verifying.
     */
    private final String TOKEN_KEY;

    /**
     * Prefix for the JWT tokens
     */
    private final String TOKEN_PREFIX = "Bearer ";

    /**
     * Standard constructor.
     */
    public TokenServiceImpl(@Value("${app.security.secret}") String tokenSecret) {
        this.TOKEN_KEY = CodingUtils.encodeToBase64(tokenSecret);
    }

    @Override
    public String generateToken(UserDetails userDetails) {
        return TOKEN_PREFIX + Jwts.builder()
            .setSubject(userDetails.getEmail())
            .setIssuedAt(Date.from(Instant.now()))
            .setId(UUID.randomUUID().toString())
            .claim("rol", userDetails.getAuthorities())
            .claim("uid", userDetails.getId())
            .signWith(SignatureAlgorithm.HS256, TOKEN_KEY)
            .compact();
    }

    @Override
    public JwtToken parseToken(String rawToken) throws SignatureException {
        Claims claims = getClaims(withoutBearer(rawToken));
        return new JwtToken(
            rawToken,
            claims.getSubject(),
            claims.get("rol").toString(),
            claims.getId(),
            claims.getIssuedAt(),
            ((Integer) claims.get("uid")).longValue()
        );
    }

    @Override
    public String getTokenId(String rawToken) {
        return getClaims(withoutBearer(rawToken)).getId();
    }

    @Override
    public String getTokenPrefix() {
        return TOKEN_PREFIX;
    }

    /**
     * Retrieves body (claims) section of the passed jwt token.
     *
     * @param rawToken JWT token in string form from which claims should be retrieved
     * @return Claims of the passed token
     */
    private Claims getClaims(String rawToken) {
        return Jwts.parser().setSigningKey(TOKEN_KEY).parseClaimsJws(rawToken).getBody();
    }

    /**
     * Strips any used token prefix from provided JWT token.
     *
     * @param rawToken token with prefix
     * @return token without prefix
     */
    private String withoutBearer(String rawToken) {
        return rawToken.substring(TOKEN_PREFIX.length());
    }
}
