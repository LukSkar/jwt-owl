package com.fatowlstudio.security.service.impl;

import com.fatowlstudio.security.model.JwtToken;
import com.fatowlstudio.security.service.AuthService;
import com.fatowlstudio.security.service.PermissionEvaluator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * Default implementation of the {@link PermissionEvaluator}. See it for public API documentation.
 */
@Service(value = "permissionEvaluator")
@Transactional
@Scope("prototype")
public class PermissionEvaluatorImpl implements PermissionEvaluator {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    private final boolean authenticatedUser;
    private final List<String> authorities;
    private final long requestingUserId;

    public PermissionEvaluatorImpl(AuthService authService) {
        JwtToken loggedUser = authService.getLoggedUserPrincipal();
        this.authenticatedUser = loggedUser != null;
        this.authorities = loggedUser != null ? getAuthoritiesAsList(authService.getLoggedUserPrincipal().getAuthorities()) : null;
        this.requestingUserId = loggedUser != null ? loggedUser.getUserId() : -1L;
        LOGGER.trace("Created new permission evaluator");
    }

    @Override
    public boolean anyOfRoles(String... roles) {
        return authenticatedUser && Arrays.stream(roles).anyMatch(authorities::contains);
    }

    @Override
    public boolean self(long targetId) {
        return authenticatedUser && requestingUserId == targetId;
    }

    @Override
    public boolean selfAndAnyOfRoles(long targetId, String... roles) {
        return self(targetId) && anyOfRoles(roles);
    }

    @Override
    public boolean selfOrAnyOfRoles(long targetId, String... roles) {
        return self(targetId) || anyOfRoles(roles);
    }

    @Override
    public boolean anyAuthenticated() {
        return authorities != null && authorities.size() > 0;
    }

    /**
     * Converts authorities comma separated string into list of those authorities. Eg. "USER, ADMIN" string will be converted
     * to list ["USER", "ADMIN"].
     *
     * @param authorities authorities string to be converted to list of authorities
     * @return list of authorities
     */
    private List<String> getAuthoritiesAsList(String authorities) {
        return Arrays.asList(authorities.replace(" ", "").split(","));
    }
}
