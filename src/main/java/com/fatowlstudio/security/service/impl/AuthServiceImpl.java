package com.fatowlstudio.security.service.impl;

import com.fatowlstudio.core.model.UserDetails;
import com.fatowlstudio.core.repository.UserDetailsRepository;
import com.fatowlstudio.security.model.JwtToken;
import com.fatowlstudio.security.model.LoginRequest;
import com.fatowlstudio.security.model.TokenStorage;
import com.fatowlstudio.security.service.AuthService;
import com.fatowlstudio.security.service.TokenService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation for {@link com.fatowlstudio.security.service.AuthService} interface. See the interface for
 * public API documentation.
 */
@Service
@Transactional
public class AuthServiceImpl implements AuthService {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    private final UserDetailsRepository userDetailsRepository;

    private final TokenService tokenService;

    /**
     * Standard constructor.
     *
     * @param tokenService autowired tokenService
     * @param userDetailsRepository autowired userDetailsRepository
     */
    public AuthServiceImpl(TokenService tokenService, UserDetailsRepository userDetailsRepository) {
        this.tokenService = tokenService;
        this.userDetailsRepository = userDetailsRepository;
    }

    @Override
    public String authorize(LoginRequest loginRequest) {
        return authorizeLoginPassword(loginRequest);
    }

    @Override
    public void logout() {
        TokenStorage.removeTokenFromStore(getLoggedUserPrincipal().getToken());
    }

    @Override
    public JwtToken getLoggedUserPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null ? (JwtToken) authentication.getPrincipal() : null;
    }

    /**
     * Authorizes user based on its login and password.
     *
     * @param loginRequest login request containing login and password
     * @return token if user successfully authorized
     */
    private String authorizeLoginPassword(LoginRequest loginRequest) {
        UserDetails userDetails = userDetailsRepository.findByEmail(loginRequest.getLogin());

        if (userDetails == null || !BCrypt.checkpw(loginRequest.getPassword(), userDetails.getPassword()))
            return null;

        return generateTokenAndLogIn(userDetails);
    }

    /**
     * Generates token for user with provided user details and logs that user in.
     *
     * @param userDetails details of user for which token should be generated
     * @return jwt token for passed user
     */
    private String generateTokenAndLogIn(UserDetails userDetails) {
        String generatedToken = tokenService.generateToken(userDetails);
        TokenStorage.putTokenInStore(generatedToken, userDetails.getId());
        return generatedToken;
    }
}
