package com.fatowlstudio.security.service;

/**
 * Interface for advanced code based permission evaluation. It is used instead of complicated spEL expressions or when
 * database query is needed due to evaluate expression.
 */
public interface PermissionEvaluator {

    /**
     * Permits requesting user to take action only if he has at least one of the roles specified in roles parameter.
     *
     * @param roles roles against which user should be checked
     * @return true if user has rights to take requested action, false otherwise
     */
    boolean anyOfRoles(String... roles);

    /**
     * Permits requesting user to take action only if requesting user id is equal to the passed target id. It's used
     * mostly as the helper method for {@link this#selfAndAnyOfRoles} and {@link this#selfOrAnyOfRoles(long, String...)}
     * but may be used independently.
     *
     * @param targetId id of the target of this request
     * @return true if user has rights to take requested action, false otherwise
     */
    boolean self(long targetId);

    /**
     * Permits requesting user to take action only if he has at least one of the roles specified in roles parameter AND
     * requesting user id is equal to the passed target id. It is used for example when user wants to update some data,
     * but he/she should be able to use only his/her own data.
     *
     * @param targetId id of the target of this request
     * @param roles roles against which user should be checked
     * @return true if user has rights to take requested action, false otherwise
     */
    boolean selfAndAnyOfRoles(long targetId, String... roles);

    /**
     * Permits requesting user to take action only if he has at least one of the roles specified in roles parameter OR
     * requesting user id is equal to the passed target id. It is used for example when user wants to delete something,
     * but as a regular user he/she should be able to delete only own data, while admin should be able to delete any data.
     *
     * @param targetId id of the target of this request
     * @param roles roles against which user should be checked
     * @return true if user has rights to take requested action, false otherwise
     */
    boolean selfOrAnyOfRoles(long targetId, String... roles);

    /**
     * Permits requesting user to take action if he is authenticated (has any role).
     *
     * @return true if user has rights to take requested action, false otherwise
     */
    boolean anyAuthenticated();
}
