package com.fatowlstudio.security.service;

import com.fatowlstudio.core.model.UserDetails;
import com.fatowlstudio.security.model.JwtToken;

/**
 * Service responsible for operations on jwt tokens.
 */
public interface TokenService {

    /**
     * Generates JWT token for user with provided details.
     *
     * @param userDetails details of the user for which token should be created
     * @return JWT token for provided user
     */
    String generateToken(UserDetails userDetails);

    /**
     * Parses provided JWT token into object representing that JWT token or
     * throw ex in case of token being malformed.
     *
     * @param rawToken JWT token in its raw string form
     * @return JWT token object representing passed rawToken
     */
    JwtToken parseToken(String rawToken);

    /**
     * Retrieves token id (jit claim) from the raw token.
     *
     * @param rawToken raw token from which id should be retrieved
     * @return jit claim of the passed token
     */
    String getTokenId(String rawToken);

    /**
     * Returns prefix used for JWT tokens
     *
     * @return prefix used for JWT tokens
     */
    String getTokenPrefix();
}
