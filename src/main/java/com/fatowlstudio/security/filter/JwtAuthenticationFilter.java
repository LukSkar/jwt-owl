package com.fatowlstudio.security.filter;

import com.fatowlstudio.security.config.WebSecurityConfig;
import com.fatowlstudio.security.model.JwtTokenAuthorizationRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Authentication filter responsible for extracting JWT token from request and passing it to authentication manager for
 * validation. It will fill Spring Security context with authenticated Authentication object in case of successful
 * authentication or clear security context otherwise.
 */
public class JwtAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    /**
     * Standard constructor. Paths that should be protected by this filter are set in call to the superclass constructor.
     */
    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        super("/**");
        setAuthenticationManager(authenticationManager);
    }

    /**
     * Attempts to authenticate user. It will try to authenticate all requests to all endpoints besides those declared
     * as 'permit all' in the {@link WebSecurityConfig}. Authentication is based on JWT token passed in the X-Authorization
     * HTTP header and with {@link com.fatowlstudio.security.provider.JwtAuthenticationProvider} provider. Based on
     * authentication result it will pass security chain execution to appropriate success/failure handler.
     *
     * @param request http request
     * @param response http response
     * @return authenticated Spring Security authentication object in case of successful authentication
     * @throws AuthenticationException in case of authentication failure
     * @throws IOException in case of io error
     * @throws ServletException in case of servlet error
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {
        String rawToken = request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER);
        return getAuthenticationManager().authenticate(new JwtTokenAuthorizationRequest(rawToken));
    }

    /**
     * Processes successfully authenticated request. Basically it will fill security context with authentication result
     * retrieved from {@link JwtAuthenticationFilter#attemptAuthentication} method and pass request for further filtering.
     *
     * @param request http request
     * @param response http response
     * @param chain security chain
     * @param authResult result of the successful authentication
     * @throws IOException in case of IO error
     * @throws ServletException  in case of servlet error
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
            Authentication authResult) throws IOException, ServletException {
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        securityContext.setAuthentication(authResult);
        SecurityContextHolder.setContext(securityContext);
        chain.doFilter(request, response);
    }

    /**
     * Process unsuccessfully authenticated request. Basically it will clear security context and return 401 UNAUTHORIZED
     * HTTP status.
     *
     * @param request http request
     * @param response http response
     * @param authException authorization ex
     * @throws IOException in case of IO error
     * @throws ServletException in case of servlet error
     */
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
            throws IOException, ServletException {
        LOGGER.trace("Request failed authentication: " + request.getServletPath());
        SecurityContextHolder.clearContext();
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
    }
}
