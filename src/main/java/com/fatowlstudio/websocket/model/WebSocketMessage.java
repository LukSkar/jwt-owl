package com.fatowlstudio.websocket.model;

/**
 * Model class for messages transferred through web sockets.
 */
public class WebSocketMessage {

    private String message;

    public WebSocketMessage() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
