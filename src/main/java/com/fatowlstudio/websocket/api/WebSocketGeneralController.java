package com.fatowlstudio.websocket.api;

import com.fatowlstudio.websocket.model.WebSocketMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * General purpose websocket controller for basic stomp endpoints like general broadcasting and user to user destinations.
 */
@Controller
@ControllerAdvice
public class WebSocketGeneralController {

    @MessageMapping("/general")
    @SendTo("/queue/general")
    public WebSocketMessage broadcast(WebSocketMessage message) {
        return message;
    }
}
