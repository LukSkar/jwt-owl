package com.fatowlstudio.websocket.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.concurrent.DefaultManagedTaskScheduler;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.messaging.StompSubProtocolErrorHandler;

/**
 * Configuration for web sockets over STOMP with embedded Spring's broker. It will register messaging endpoints and
 * delegate handling messages to appropriate Spring's message mapping controllers.
 */
@Configuration
@EnableWebSocketMessageBroker
@Order(Ordered.HIGHEST_PRECEDENCE + 99)
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    // WebSocket handshake endpoint
    public static final  String WS_HANDSHAKE = "/ws/**";
    private static final String WS_ENDPOINT = "/ws";
    private static final String BROKER_DEST_PREFIX = "/queue";
    private static final String APP_DEST_PREFIX = "/app";

    // Autowired messaging interceptor, needed for STOMP security. This is done by field injection to avoid rewriting
    // other security sections (as otherwise it would be needed to handle token service in authorization manager differently,
    // which may be good idea anyway but was out of scope when STOMP security was added).
    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    private StompMessagingInterceptor messagingInterceptor;

    @Override
    public void configureMessageBroker(MessageBrokerRegistry broker) {
        broker
            .enableSimpleBroker(BROKER_DEST_PREFIX)
            .setHeartbeatValue(new long[]{30000L, 30000L})
            .setTaskScheduler(new DefaultManagedTaskScheduler());

        broker.setApplicationDestinationPrefixes(APP_DEST_PREFIX);
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry stompRegistry) {
        stompRegistry
            .addEndpoint(WS_ENDPOINT)
            .setAllowedOrigins("*")
            .withSockJS()
            .setHeartbeatTime(30000L);
    }

    @Override
    public void configureClientInboundChannel(ChannelRegistration channelRegistry) {
        channelRegistry.interceptors(messagingInterceptor);
    }
}
