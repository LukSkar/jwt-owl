package com.fatowlstudio.websocket.config;

import com.fatowlstudio.security.model.JwtTokenAuthorizationRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.stereotype.Component;

/**
 * Interceptor class for Spring's STOMP WebSockets. It will intercept messages BEFORE they are send further into channel,
 * so message can be modified, eg. user authentication can be set before passing message to process by Spring.
 */
@Component
public class StompMessagingInterceptor extends ChannelInterceptorAdapter {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    private final AuthenticationProvider authenticationProvider;

    public StompMessagingInterceptor(AuthenticationProvider authenticationProvider) {
        this.authenticationProvider = authenticationProvider;
    }

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        // Obtain header accessor used for further message operations
        StompHeaderAccessor headerAccessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        // Make sure heartbeat messages are permitted as they are (no modifications should be made)
        if (isHeartbeat(headerAccessor)) return message;

        switch (headerAccessor.getCommand()) {
            // Authenticate STOMP session on CONNECT using jwt token passed as a STOMP login header
            case CONNECT:
                authorizeStompSession(headerAccessor);
                break;
        }

        // Returns processed message
        return message;
    }

    /**
     * Checks if passed message is STOMP heartbeat message. Message should be passed wrapped in StompHeaderAccessor class.
     * (Use eg. {@link MessageHeaderAccessor#getAccessor(Message, Class)} to wrap). Will return true if so or false in any
     * other case.
     *
     * @param headerAccessor header accessor wrapping message to be checked
     * @return true if passed message is a heartbeat message
     */
    private boolean isHeartbeat(StompHeaderAccessor headerAccessor) {
        // Make sure heartbeat messages are permitted as they are
        return headerAccessor.getHeader(SimpMessageHeaderAccessor.MESSAGE_TYPE_HEADER) != null &&
               headerAccessor.getHeader(SimpMessageHeaderAccessor.MESSAGE_TYPE_HEADER).equals(SimpMessageType.HEARTBEAT);
    }

    /**
     * Will authenticate and authorize user's STOMP session. To successfully authorize header accessor should be passed.
     * This header accessor needs to wrap STOMP message containing user's jwt token as a STOMP login header. To wrap message
     * use eg. {@link MessageHeaderAccessor#getAccessor(Message, Class)}.
     *
     * @param headerAccessor header accessor wrapping message with user's authentication data
     */
    private void authorizeStompSession(StompHeaderAccessor headerAccessor) {
        String jwtToken = headerAccessor.getLogin();
        headerAccessor.setUser(authenticationProvider.authenticate(new JwtTokenAuthorizationRequest(jwtToken)));
        LOGGER.info("Authorized STOMP session for user: " + headerAccessor.getUser().getName());
    }
}
