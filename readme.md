## JWT OWL Template

JWT OWL Template is a java application template that provides ready to use JWT support for REST services build on top
of the Spring Boot with some additional out-of-the-box functions like registration/login system, CORS support, WebSocket
over STOMP support etc. You can clone this repository and add your controllers that will be automatically secured by 
Spring Security validating requests by JWT token. See 'How to use' section for more details.

### Technological stack:

The template is build on top of the following technological stack:

- Java 9
- [Spring Boot](https://projects.spring.io/spring-boot/) (2.0.1) as a foundation of the template (with some basic
    starters needed for web app development, see source code pom.xml)
- Tomcat as a servlet container (embedded version provided by Spring Boot)
- Spring Security as a security framework, enchanted to support JWT tokens
- [JJWT](https://github.com/jwtk/jjwt) library for creating, singing and parsing JWT tokens
- MySQL and Hibernate (provided by Boot starter) as a default underlying database/ORM layer
- Spring WebSockets module for providing support for WebSockets over SockJS/STOMP subprotocol

## How to use

### Setup and general info

First clone this repository and create folders `src/main/properties/dev` and `src/main/properties/prod` inside it. Copy file 
`src/main/properties/setup/application.properties.setup` to both new folders without '.setup' suffix so Spring can auto-load 
those files as a configuration settings. Fill them up with appropriate settings for development (file in `dev` folder) and 
production environment (`prod` folder). Appropriate file will be included in the `mvn build` resultant jar depending on 
profile with which jar was built. By default `dev` profile is active. If you want to use production one use `-Pprod` mvn
switch while building. Make sure to provide your own, strong password for the `app.security.secret` property as it is used 
to sign and verify JWT tokens. Start the application so the default UserDetails model will be created by Hibernate in your 
database (template is set up to work with mySQL, but you can change underlying database - everything is configured with 
standard Spring Boot procedure. You can also create that model by yourself, Hibernate will create it only if it doesn't 
find user_details table in your db). UserDetails contains self-explanatory fields: `id`, `email`, `password` and `authorities`. 
`password` is a BCrypt hash (standard Spring Security BCrypt implementation is used for hash comparing). `authorities` is 
comma separated string containing list of authorities of a user (eg. "ADMIN, MANAGER, TESTER"). Fill database with your 
users or create them using included registration logic (see _Registration_ section for more info). By default all new users 
are created with `USER` authority.

Run template - it is fully working REST webapp with JWT user authentication, see sections below for more details on 
authentication process.

### Registration
You can by default register new users by sending HTTP POST request to the `/api/users` with content body containing email
of the registered user and content type set to the `application/json`, ie.:

```json
{
    "email":"example@google.com"
}
```

User with that email will be registered with randomly generated password and `USER` role by default if email is available.
User's password will be sent to him/her on the email address passed during registration. Make sure your properties files 
have properly configured ``EMAILS`` sections so the email can be sent (there is no other way to obtain initial password 
than receiving it by email).

### Logging in, out and token format

Logging in means generating token for user based on his/her login and password. Send a request to the unprotected
`/api/auth/login` endpoint with content type set to `application/json` and request body in form of:

```json
{
    "login":"user_login",
    "password":"user_password"
}
```

If everything matches a JWT token will be sent in response in the form of `Bearer JWT_TOKEN`, wrapped in JSON object in field `value` eg.:
```json
{
    "value":"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
}
```

Otherwise either HTTP `401 UNAUTHORIZED` status will be returned in case of invalid login/password or `409 CONFLICT` if 
account with chosen email already exist and there is no way to create new one. When sending token to the server you should 
always be using full token, it is with the "Bearer " part.

Be sure to accept `application/json` responses from login endpoint.

Generated token will be stored on the server to enable logging out or token revoking. **This is not stateless, but this 
template does not aim to be stateless - you can always remove `TokenStorage` class and all put/get operations to it,
but at the current template state it will make tokens valid forever - even after server restart! (or at least until you 
change your secret).**

To logout you should call `/api/auth/logout` providing your token as a `X-Authorization` request header (as in every other 
protected endpoint). Provided token will be removed from the server, making it unusable, effectively logging user off.

### Authenticating secured requests

Every controller marked as a `@RestController` will be secured by Spring Security. If no other security rules are provided 
(you can use all of Spring Security features like `@PreAuthorize` or `@AuthenticationPrincipal` etc.) controller methods 
will have `isAuthenticated()` rule applied by default. If you want to make endpoint publicly available you should add it 
to ignoring list of the `WebSecurityConfig#configure(WebSecurity)` method (see login endpoint as an example). **IMPORTANT:** 
controller methods have to be **public** in order to work with Spring Security. Public modificator has to be written explicitly -
common mistake may be leaving controller methods as package-private, in this case Spring Security won't be able to secure 
those endpoints!

Authentication is done based on JWT token that should be provided in each request to a secured endpoint in the
`X-Authorization` header. This token will be extracted, checked if server knows it and if its signature is valid. If
everything is ok Spring Security Context will be filled with roles defined in the provided token and Spring Security will
further determine if user is allowed to obtain requested resource in the standard way. In case of user being not
authenticated or not allowed HTTP `401 UNAUTHORIZED` or `403 FROBIDDEN` status will be returned.

For more fine-grained or logic-heavy authentication rules custom ``PermissionEvaluator`` can be used. It provides some 
custom security expression methods like eg. ``self(targetId)`` to validate if user with target passed id is the same user
 who is making request. You can use those methods with standard ``@PreAuthorize`` annotation, eg. 
 ``@PreAuthorize("@permissionEvaluator.self(targetId)")``. For more information see ``PermissionEvaluator`` javadoc.

### Other out-of-the-box functions

In addition to authorization this template provides some commonly used features, like:

* Swagger UI - You can test endpoints simply by accesing autogenerated UI at `/swagger-ui.html` endpoint. Remember to turn
this endpoint off for production (by setting `app.swagger.enabled` property in `application.properties` to `false`).

* Registration. See _Registration_ and _Logging in, out and token format_ sections for more information.

* Basic WebSockets support. See _WebSockets_ section for more information.

## WebSockets

Template exposes working WebSocket over STOMP subprotocol. You can connect to it on ``http://host:port/ws`` WebSocket 
endpoint, from browser preferably with SockJS and StompJS which will ensure full compatibility and fallback options. 
Example of working browser websocket client can be found [here](https://spring.io/guides/gs/messaging-stomp-websocket/). 
While it is recommended approach exposed socket link is just a WebSocket, which can be handled by any other WS framework 
or custom implementation.

After establishing physical connection user should send STOMP CONNECT frame with login header set to his/her JWT token 
in order to authenticate and being allowed do process further WebSocket communication. You can read more about STOMP
frames on it's [documentation page](https://stomp.github.io/). After user is connected and authorized there are two 
endpoints that can be subscribed:

- ``/queue/general`` - it's general broadcasting channel.
- ```/user/queue/priv``` - user's private channel. As it looks like generic endpoint available to everyone, in fact subscribing
to ``/user/**`` will create separate private channel targeting only session from which subscription was made.

and accoridingly two endpoints to which message can be send:

- ``/app/general`` - sends message to the general broadcasting channel. Everyone subscribed to ``/queue/general`` will receive
sent message.
- ``/user/{userName}/queue/priv`` - sends private message to the user with ``userName``. If nothing was modified by default
user name is this user email address, do to send private message eg. to example@test.com message should be sent on the 
``/user/example@test.com/queue/priv``. Only user with this email address will receive this message. Receiving user needs
to be subscribing to it's private subscribe endpoint (see above).

All other endpoints are protected and messaging through them isn't possible. It is recommended to subscribe to both available
subscribe endpoints when user logs in. STOMP body of the message is regular JSON, by default representing object showed below:

```json
{
    "message":"<message_to_be_sent>"
}
```

## How does it works

This template basically adds implementations of the Spring Security `AbstractAuthenticationProcessingFilter` and
`AuthenticationProvider` classes to validate JWT tokens and fill Spring Security Context with result of this validation.
Those classes are injected to Spring Security chain and configured in `WebSecurityConfigurerAdapter` implementation.

If you want to edit authorization/authentication behaviour you should check `AuthServiceImpl` and `JwtAuthenticationProvider`
classes where validation _magic_ happen.

Base solution was created mostly (but not only) based on those awesome articles found on the web, so check them out if you need 
better source code understanding (or read javadoc - everything is documented):

- [JWT Authentication Tutorial by Vladimir Stankovic](http://www.svlada.com/jwt-token-authentication-with-spring-boot/)
- [REST Security with JWT using Java and Spring Security by Dejan Milosevic](https://www.toptal.com/java/rest-security-with-jwt-spring-security-and-java)

This template started as a simple ready to deploy REST template with only JWT support added, but as it evolved some new 
functions were added and some not-so-great or badly written features were removed. A lot of this stuff were made based on 
documentations, tutorials, blog posts and great StackOverflow questions and referencing all of them here would be impractical,
but if you have questions feel free to contact me - I can provide aforementioned materials on which things were made or 
support you as best as I can. You can find contact info at the end of this readme.

## To do

Those are things that would be nice to have:

- Unit (or any other type) tests.
- Implement refresh token mechanism. It would allow to make usage of jwt expiration dates and store
in memory only revoked tokens until they expire, it would reduce need for memory and increase security
(no forever valid tokens).
- Refactor `JwtAuthenticationProvider` so it won't need to use warnings suppression.
- Enable Spring Boot Actuator. It is disabled right now due to problems with security configuration. Desired way of actuator
endpoints to work is that they are fully available for users with defined roles and non-available at all for others.
- CORS support for actuator.
- Turn default security user with auto-generated password off.
- Make not annotated controller methods not available. It would force programmer to explicity usage of security annotation,
minimalising risk of accidentaly leaving endpoint without proper protection.
- Secure WebSocket SockJS HTTP handshake. At the moment it is publicly available and authorization happens only after the STOMP 
CONNECT command received, which means that some 'wild' connections may open and consume bandwitch without being detected. 
However this is more complicated problem, see [this StackOverflow post](https://stackoverflow.com/questions/30887788/json-web-token-jwt-with-spring-based-sockjs-stomp-web-socket) 
which explains problem in details.
- Find a way to programatically close WebSocket connection. As it is possible, it would require messing up with Spring-WebSockets
internals to get access to WebSocketSession, so it should be separate task. It would enable us to store sessions wit their 
ids and close them on demand (eg. when user to which session belongs logged out but didn't close browser window).
- Work to improve spring-security-messaging exceptions handling (see 
[this StackOverflow post](https://stackoverflow.com/questions/48688798/how-to-globally-handle-spring-websockets-spring-messaging-exception)).
- Check and possibly leverage new features offered by Spring Boot 2 and Java 9 - as template was moved to fully support them 
it for the most part doesn't use new features.

## Licence

Only for Fat Owl Studio company usage. In case of any questions please write to lukskar@gmail.com
